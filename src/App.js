import logo from './logo.svg';
import './App.css';

function App() {
    const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    const arrayOfweekdays = weekdays.map((name)=>{

    return (
      <tr>
        <td>{name}</td>
      </tr>
    );

  });
  return (
    <div className="App">
      <table>
        <thead>
          <tr>
            <th>Weekday</th>
          </tr>
        </thead>
     <tbody>{arrayOfweekdays}</tbody>
      </table>
    </div>
  );
}

export default App;
